let mix = require('laravel-mix');

mix.sass('resources/sass/index.scss', 'css/index.min.css')
   .js('resources/js/index.js', 'js/index.min.js')
   .setPublicPath('public');
