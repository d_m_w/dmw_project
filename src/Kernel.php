<?php

namespace App;

use Dmw\Core\Kernel\Kernel as KernelBase;
use Dmw\Core\Console\Interfaces\SchedulerInterface;
use Dmw\Component\Contracts\Kernel\ContainerInterface;
use Dmw\Component\Contracts\Kernel\KernelExceptionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class Kernel extends KernelBase
{
    /**
     * Lista de middlewares da pasta vendor
     * @var array
     */
    protected $middlewaresWeb = [
        'csrf' => \Dmw\Core\Sessions\Middlewares\CsrfMiddleware::class,
        'recaptcha' => \Dmw\Core\Captcha\Middlewares\ReCaptchaMiddleware::class,
        'language' => \Dmw\Component\Language\Middlewares\LanguageMiddleware::class,
        //'oauth2' => \Dmw\Component\OAuth2\Middlewares\OAuth2Middleware::class
    ];

    /**
     * Lista de middlewares da pasta vendor
     * @var array
     */
    protected $middlewaresApi = [

    ];

    /**
     * Executa tarefas
     */
    public function schedule(SchedulerInterface $scheduler): void
    {
        //$scheduler->raw('app:command', ['--param' => 'value'])->everyMinute(5);
    }

    /**
     * Tratar excessões
     * @param ContainerInterface       $container
     * @param KernelExceptionInterface $kernelException
     */
    protected function onException(
        ContainerInterface $container,
        KernelExceptionInterface $kernelException
    ): void {

    }
}
