<?php

namespace App\Controller;

use Dmw\Core\Routing\Controller;
use Dmw\Component\Contracts\Language\LangInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class TestController extends Controller
{
    /**
     * @var LangInterface
     */
    private $lang;

    /**
     * @param LangInterface $lang
     */
    public function __construct(
        LangInterface $lang
    ) {
        $this->lang = $lang;
    }

    /**
     * @param  Request $request PSR7
     * @return Response
     */
    public function index(
        Request $request
    ): Response {
        $params   = [
            'message' => $this->lang->trans('Welcome do DMW Framework')
        ];

        return $this->view('pages/index', $params);
    }
}
