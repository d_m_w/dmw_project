# DMW framework

Este projeto auxilia na utilização da DMW framework para que você explore ao máximo todos os recursos disponíveis.

## 💡 Requisitos

PHP 8.1 ou superior

## 💻 Instalação

No diretório de seu projeto, execute em seu terminal o seguinte comando:

```shell
composer create-project dmw/dmw [PROJECT_NAME]
```

Após instalar, renomeie o arquivo ``.env.example`` para ``.env``

Em seguida, altere o valor da variável ``DB_URL`` colocando as credenciais do seu banco de dados.

Pronto!! Você já pode utilizar todos os recursos para acelerar o desenvolvimento do seu projeto.

Para explorar a lista de comandos disponíveis. Em seu terminal, acesse a pasta do projeto e digite o seguinte comando:

```shell
php tony list
```

## 🏻 Licença de uso

Consulte o arquivo LICENSE para obter mais informações.
