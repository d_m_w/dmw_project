<?php

use Dmw\Core\Kernel\Environment;
use Dmw\Core\Configuration\Schema;

return [
    'params' => [
        'app_env' => Environment::env('APP_ENV'),
        'jobs' => [
            'deleteAfter' => 7,
            'cache' => [
                'success' => 'kernel_schedule',
                'failed' => 'kernel_schedule_failed'
            ]
        ]
    ],
    'schema' => Schema::create([
      'app_env' => Schema::anyOf('dev', 'prod')->required(),
      'jobs' => Schema::array([
          'deleteAfter' => Schema::int()->required(),
          'cache' => Schema::array([
              'success' => Schema::string()->required(),
              'failed' => Schema::string()->required()
          ])
      ])
    ])
];
