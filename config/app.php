<?php

use Dmw\Core\Kernel\Environment;
use Dmw\Core\Configuration\Schema;

return [
    'params' => [
        'timezone' => '+00:00', //UTC
        'language' => 'pt-br',
        'key' => Environment::env('CRYPTO_KEY')
    ],
    'schema' => Schema::create([
        'timezone' => Schema::string()->required(),
        'language' => Schema::anyOf('en', 'pt-br', 'auto'),
        'key' => Schema::string()->required()
    ])
];
