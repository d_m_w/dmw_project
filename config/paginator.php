<?php

use Dmw\Core\Configuration\Schema;

return [
    'params' => [
        'ul-class' => 'pagination justify-content-center',
        'li-class' => 'page-item',
        'a-class' => 'page-link',
        'class-active' => 'active',
        'class-disabled' => 'disabled',
        'previous-text' => '&laquo;',
        'next-text' => '&raquo;',
        'max-items-per-page' => 100,
        'items-per-page' => 10,
        'max-pages-to-show' => 5
    ],
    'schema' => Schema::create([
        'ul-class' => Schema::string()->required(),
        'li-class' => Schema::string()->required(),
        'a-class' => Schema::string()->required(),
        'class-active' => Schema::string()->required(),
        'class-disabled' => Schema::string()->required(),
        'previous-text' => Schema::string()->required(),
        'next-text' => Schema::string()->required(),
        'max-items-per-page' => Schema::int()->required(),
        'items-per-page' => Schema::int()->required(),
        'max-pages-to-show' => Schema::int()->required()
    ])
];
