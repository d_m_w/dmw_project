<?php

use Dmw\Core\Kernel\Environment;
use Dmw\Core\Configuration\Schema;

return [
    'params' => [
        'default' => [
            'driver' => 'mysql',
            'dsn' => Environment::env('DB_URL')
        ],
        'db2' => [
            'driver' => 'mysql',
            'dsn' => ''
        ],
        'redis' => [
            'driver' => 'redis',
            'dsn' => Environment::env('DB_REDIS')
        ]
    ],
    'schema' => Schema::create([
        'default' => Schema::array([
            'driver' => Schema::anyOf('mysql', 'sqlite', 'postgresql', 'oci8', 'firebird')->required(),
            'dsn' => Schema::string()->required()
        ]),
        'db2' => Schema::array([
            'driver' => Schema::anyOf('mysql', 'sqlite', 'postgresql', 'oci8', 'firebird')->required(),
            'dsn' => Schema::string()
        ]),
        'redis' => Schema::create([
            'driver' => Schema::anyOf('redis')->required(),
            'dsn' => Schema::string()->required()
        ])
    ])
];
