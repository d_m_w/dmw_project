<?php

use Dmw\Core\Kernel\Environment;
use Dmw\Core\Configuration\Schema;

return [
    'params' => [
        'driver' => 'native',
        'params' => [
            'session' => [
                'name' => 'dmw_session',
                'use_cookies' => 'On',
                'use_only_cookies' => 'On',
                'use_strict_mode' => 'On',
                'cookie_httponly' => 'On',
                'use_trans_sid' => 'On',
                'sid_bits_per_character' => 6,
                'hash_function' => 'sha256',
                'cookie_lifetime' => 86400, //1 dia
                'cookie_domain' => Environment::env('SESSION_DOMAIN'),
                'gc_maxlifetime' => 86400, //1 dia
                'gc_probability' => 1,
                'gc_divisor' => 100
            ],
            'csrf' => [
                'ignore_routes' => [
                    '/user/login/google/request',
                    '/user/login/faceboook/request'
                ]
            ]
        ]
    ],
    'schema' => Schema::create([
        'driver' => Schema::anyOf('native', 'cache')->required(),
        'params' => Schema::array([
            'session' => Schema::array([
                'name' => Schema::string()->required(),
                'use_cookies' => Schema::string()->required(),
                'use_only_cookies' => Schema::string()->required(),
                'use_strict_mode' => Schema::string()->required(),
                'cookie_httponly' => Schema::string()->required(),
                'use_trans_sid' => Schema::string()->required(),
                'sid_bits_per_character' => Schema::int()->required(),
                'hash_function' => Schema::string()->required(),
                'cookie_lifetime' => Schema::int()->required(),
                'cookie_domain' => Schema::string()->required(),
                'gc_maxlifetime' => Schema::int()->required(),
                'gc_probability' => Schema::int()->required(),
                'gc_divisor' => Schema::int()->required()
            ]),
            'csrf' => Schema::array([
                'ignore_routes' => Schema::array([])
            ])
        ])
    ])
];
