<?php

use Dmw\Core\Kernel\Environment;
use Dmw\Core\Configuration\Schema;

return [
    'params' => [
        'driver' => 'phpmailer',
        'from' => [
            'name' => '',
            'email' => ''
        ],
        'drivers' => [
            'phpmailer' => [
                'debug' => false,
                'timezone' => 'Etc/UTC',
                'charset' => 'UTF-8',
                'send' => [
                    'protocol' => 'smtp',
                    'dsn' => Environment::env('SEND_URL')
                ],
                'receive' => [
                    'protocol' => 'imap',
                    'dsn' => Environment::env('RECEIVE_URL')
                ]
            ],
            'sendgrid' => [
                'api_key' => Environment::env('SENDGRID_API_KEY')
            ]
        ]
    ],
    'schema' => Schema::create([
        'driver' => Schema::anyOf('phpmailer', 'sendgrid')->required(),
        'from' => Schema::array([
            'name' => Schema::string()->required(),
            'email' => Schema::string()->required()
        ]),
        'drivers' => Schema::array([
            'phpmailer' => Schema::array([
                'debug' => Schema::bool()->required(),
                'timezone' => Schema::string()->required(),
                'charset' => Schema::string()->required(),
                'send' => Schema::array([
                    'protocol' => Schema::anyOf('smtp')->required(),
                    'dsn' => Schema::string()
                ]),
                'receive' => Schema::array([
                    'protocol' => Schema::anyOf('imap')->required(),
                    'dsn' => Schema::string()
                ])
            ]),
            'sendgrid' => Schema::array([
                'api_key' => Schema::string()
            ])
        ])
    ])
];
