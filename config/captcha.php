<?php

use Dmw\Core\Kernel\Environment;
use Dmw\Core\Configuration\Schema;

return [
    'params' => [
        'recaptcha' => [
            'enabled' => false,
            'site_key' => Environment::env('CAPTCHA_SITE_KEY'),
            'secret_key' => Environment::env('CAPTCHA_SECRET_KEY')
        ]
    ],
    'schema' => Schema::create([
        'recaptcha' => Schema::array([
            'enabled' => Schema::bool()->required(),
            'site_key' => Schema::string(),
            'secret_key' => Schema::string()
        ])
    ])
];
