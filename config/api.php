<?php

use Dmw\Core\Configuration\Schema;

return [
    'params' => [
        'enabled' => true,
        'authentication' => false,
        'driver' => 'oauth2'
    ],
    'schema' => Schema::create([
        'enabled' => Schema::bool()->required(),
        'authentication' => Schema::bool()->required(),
        'driver' => Schema::anyOf('oauth2', 'auth')->required()
    ])
];
