<?php

use Dmw\Core\Configuration\Schema;

return [
    'params' => [
        'prefix' => 'dmw',
        'driver' => 'phpfast',
        'adapter' => 'file',
        'adapters' => [
            'file' => [
                'path' => 'storage/cache'
            ],
            'redis' => [
                'database' => 'redis'
            ]
        ]
    ],
    'schema' => Schema::create([
        'prefix' => Schema::string()->required(),
        'driver' => Schema::anyOf('symfony', 'phpfast')->required(),
        'adapter' => Schema::anyOf('file', 'redis'),
        'adapters' => Schema::array([
            'file' => Schema::array([
                'path' => Schema::string()->nullable()
            ]),
            'redis' => Schema::array([
                'database' => Schema::anyOf('redis')->required(),
            ])
        ])
    ])
];
