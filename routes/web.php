<?php

$router->middleware([
    'before' => [
        'test'
    ]
], function () use ($router) {
    $router->get('/', 'TestController::index')
        ->scope('test')
        ->description('Rota padrão')
    ;
});
